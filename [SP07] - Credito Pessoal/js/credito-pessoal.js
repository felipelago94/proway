function simularCredito(){
    let simulacao = document.querySelector(".simulacao");
    let valorParcelas = document.querySelector(".valor-parcelas");

    let valorEmprestimo = document.querySelector('input[name="valor-emp"]').value;
    let qtdParcelas = document.querySelector('input[name="qtd-parcelas"]').value;

    // peguei daqui https://pt.stackoverflow.com/questions/244083/tolocalestring-r-brasileiro
    const formatter = new Intl.NumberFormat('pt-BR', {
        style: 'currency',
        currency: 'BRL' 
    });

    if((valorEmprestimo.length > 0 && valorEmprestimo > 0) && (qtdParcelas.length > 0 && qtdParcelas > 0)) {
        // considerando 7.5% de juros ao mês e parcelas fixas
        // https://mundoeducacao.uol.com.br/matematica/tabela-price.htm
        let parcelas = valorEmprestimo*((Math.pow(1.075, qtdParcelas)*0.075)/(Math.pow(1.075, qtdParcelas)-1));

        valorParcelas.textContent = formatter.format(parcelas);
        
        simulacao.classList.remove("d-none");
    } else {
        simulacao.classList.add("d-none");
    }
}

function toggleSaldo() {
    let saldo = document.querySelector(".saldo");
    let olho = document.querySelector(".olho");

    saldo.classList.toggle("fechado");
    olho.classList.toggle("fechado");
}